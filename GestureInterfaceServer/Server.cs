using System;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;

namespace GestureInterface
{
    
    public class Server
    {
        private const int PORT = 44444;
  
        private const string OK = "ok";
        private const string ERROR = "error";
        
        private TcpListener tcpListener = new TcpListener(PORT);
        
        private void Listeners(Socket clientSocket)
        {
            
            if (clientSocket.Connected)
            {
                Console.WriteLine("> Client: " + clientSocket.RemoteEndPoint + " connected to SERVER");
                NetworkStream networkStream = new NetworkStream(clientSocket);

                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);

                string str = "";
                string request = "";
                
                while (true)
                {
                    try
                    {
                        str = streamReader.ReadLine();
                        
                        if (str == "exit" || str == "")
                            break;
                        else
                        {
                           
                            Console.WriteLine("  " + str);
                            request += str;
                            if (str == "}")
                            {
                                String ret = processRequest(request);
                                Console.WriteLine(" ---------- RETURN ----------\n" + ret + "\n ----------------------------");
                                streamWriter.WriteLine(ret);
                                streamWriter.Flush();
                                request = "";
                            }
                            
                        }
                    }
                    catch (Exception e)
                    {
                        if (e is System.IO.IOException || e is System.NullReferenceException)
                        {
                            if (!clientSocket.Connected)
                            {
                                break;
                            }
                        }
                    }
                    
                    
                }
                Console.WriteLine("> Client disconnected!");
                streamReader.Close();
                networkStream.Close();
                streamWriter.Close();
            }
            clientSocket.Close();
        }

        private String processRequest(String input)
        {
            /*  “status”: “ok”,
                “additional_info”: “7”,
                “sequential_number”: “15”,
                “timestamp”: “2018-11-15 20:30:13 CET”
            */

            try
            {
                ClientFrame client = (ClientFrame) JsonConvert.DeserializeObject<ClientFrame>(input);
                string status;
                string additional_info;
                
                status = client.status == Server.OK ? Server.OK : Server.ERROR;
                additional_info = status == Server.ERROR ? "an error occured!" : "";
                    
                ServerFrame serverFrame = new ServerFrame(status, additional_info, Convert.ToInt32(client.sequential_number), DateTime.Now);

                return JsonConvert.SerializeObject(serverFrame, Formatting.Indented);
            }
            catch (System.IO.IOException e)
            {
                return JsonConvert.SerializeObject(new ServerFrame("error", "parse exception!", -1, DateTime.Now), Formatting.Indented);

            }
        }

        public static void Main()
        {
            Server server = new Server();
            Socket socket;
            
            server.tcpListener.Start();
            
            Console.WriteLine("---- Server ----");
            while (true)
            {
                socket = server.tcpListener.AcceptSocket();
                Thread thread = new Thread(() => server.Listeners(socket));
                //Thread thread = new Thread(new ParameterizedThreadStart(server.Listeners()));
                thread.Start();
            }
        }
    }
}