using System;

using IniParser;
using IniParser.Model;

namespace GestureInterface {

    public class GIProperties {
        private const string SECTION_GESTUREINTERFACE = "GestureInterface";
        private static GIProperties properties;
        public readonly bool CONNECTION_TEST;

        public readonly int COOLDOWN_GESTURE_DETECTION_MILLISECONDS;
        public readonly int MAX_ERRORS_HANDSTATE;
        public readonly int MAX_LENGTH_HANDSTATE_FIELD;
        public readonly int MIN_HANDSTATE_CHANGE_TRIGGER;

        // const-variables for connection

        public readonly int PORT;
        public readonly int RESPONSE_TIMEOUT;
        public readonly string SERVERIP;

        // const-variables for GestureInterface.cs (GestureDetection)

        public readonly int TIME_BETWEEN_FRAMES;
        public bool LOGGING;

        public GIProperties() {
            var filename = "config\\giproperties.ini";
            var parser = new FileIniDataParser();
            var data = parser.ReadFile(filename);

            string[] properties = {
                "TIME_BETWEEN_FRAMES",
                "COOLDOWN_GESTURE_DETECTION_MILLISECONDS",
                "MIN_HANDSTATE_CHANGE_TRIGGER",
                "MAX_ERRORS_HANDSTATE",
                "MAX_LENGTH_HANDSTATE_FIELD",
                "PORT",
                "RESPONSE_TIMEOUT"
            };

            int[] defaultValues = {80, 3000, 3, 5, 15, 44444, 1000};
            int value;

            for (var i = 0; i < properties.Length; i++) {
                value = readInt(data, SECTION_GESTUREINTERFACE, properties[i]);
                if (value != -1)
                    defaultValues[i] = value;
            }

            TIME_BETWEEN_FRAMES = defaultValues[0];
            COOLDOWN_GESTURE_DETECTION_MILLISECONDS = defaultValues[1];
            MIN_HANDSTATE_CHANGE_TRIGGER = defaultValues[2];
            MAX_ERRORS_HANDSTATE = defaultValues[3];
            MAX_LENGTH_HANDSTATE_FIELD = defaultValues[4];
            PORT = defaultValues[5];
            RESPONSE_TIMEOUT = defaultValues[6];

            SERVERIP = data[SECTION_GESTUREINTERFACE]["SERVERIP"] != null
                ? data[SECTION_GESTUREINTERFACE]["SERVERIP"]
                : "192.168.43.73";
            LOGGING = Convert.ToBoolean(data[SECTION_GESTUREINTERFACE]["LOGGING"] != null
                ? data[SECTION_GESTUREINTERFACE]["LOGGING"]
                : "true");
            CONNECTION_TEST = Convert.ToBoolean(data[SECTION_GESTUREINTERFACE]["CONNECTION_TEST"] != null
                ? data[SECTION_GESTUREINTERFACE]["CONNECTION_TEST"]
                : "false");
        }

        public static GIProperties getInstance() {
            if (properties == null)
                properties = new GIProperties();
            return properties;
        }

        private int readInt(IniData data, string section, string key) {
            var text = data[section][key];
            return text != null ? Convert.ToInt32(text) : Convert.ToInt32("-1");
        }

        public override string ToString() {
            return "GIProperties:\n\nTIME_BETWEEN_FRAMES=" + TIME_BETWEEN_FRAMES +
                   "\nFRAMES_REFUSE_GESTEURE_DETECTION=" +
                   COOLDOWN_GESTURE_DETECTION_MILLISECONDS +
                   "\nMIN_HANDSTATE_CHANGE_TRIGGER=" + MIN_HANDSTATE_CHANGE_TRIGGER + "\nMAX_ERRORS_HANDSTATE=" +
                   MAX_ERRORS_HANDSTATE +
                   "\nMAX_LENGTH_HANDSTATE_FIELD=" + MAX_LENGTH_HANDSTATE_FIELD + "\nPORT=" + PORT +
                   "\nRESPONSE_TIMEOUT=" + RESPONSE_TIMEOUT + "\nCONNECTION_TEST=" + CONNECTION_TEST + "\n\n";
        }
    }

}