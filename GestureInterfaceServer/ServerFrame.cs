using System;

namespace GestureInterface
{
    class ServerFrame {
        public string status { get; set; }
        public string additional_info { get; set; }
        public int sequential_number { get; set; }
        public string timestamp { get; set; }

        public ServerFrame(string status, string additional_info, int sequential_number, DateTime timestamp) {
            this.status = status;
            this.additional_info = additional_info;
            this.sequential_number = sequential_number;
            this.timestamp = timestamp.ToString("G");
        }
    }
}