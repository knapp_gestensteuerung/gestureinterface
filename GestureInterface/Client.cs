using System.Diagnostics;
using System.IO;
using System.Net.Sockets;

using log4net;

namespace GestureInterface {

    public class Client {
        private ILog logger;
        private NetworkStream networkStream;
        private readonly GIProperties properties;
        private bool ready;

        private TcpClient socket;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private GITaskIcon taskIcon;

        public Client(GITaskIcon icon, ILog logger) {
            properties = GIProperties.getInstance();
            taskIcon = icon;
            this.logger = logger;
        }

        public void Connect() {
            try {
                socket = new TcpClient(properties.SERVERIP, properties.PORT);
                networkStream = socket.GetStream();
                streamReader = new StreamReader(networkStream);
                streamWriter = new StreamWriter(networkStream);
                ready = true;
            }
            catch {
                throw new IOException("Could not connect to server! (" + properties.SERVERIP + ":" + properties.PORT +
                                      ")");
            }
        }

        public string SendAndReceive(string s) {
            string retString = "", line;
            try {
                streamWriter.WriteLine(s);
                streamWriter.Flush();

                var stopWatch = new Stopwatch();
                stopWatch.Start();

                do {
                    line = streamReader.ReadLine();

                    var time = (int) stopWatch.Elapsed.TotalMilliseconds;
                    if (time > properties.RESPONSE_TIMEOUT)
                        return "error";

                    if (line != null)
                        retString += line + "\n";
                } while (!"}".Equals(line));

                return retString;
            }
            catch {
                throw new IOException("Failed to send message to server (" + properties.SERVERIP + ":" +
                                      properties.PORT + ")");
            }
        }

        public void Disconnect() {
            if (socket != null) {
                SendAndReceive("exit");
                socket.Close();
            }

            if (networkStream != null)
                networkStream.Close();
        }
    }

}