using System;

namespace GestureInterface {

    public class ClientFrame {
        public string event_name { get; set; }
        public string hostname { get; set; }
        public int sequential_number { get; set; }
        public string timestamp { get; set; }
        public string status { get; set; }
        public string additional_info { get; set; }

        public ClientFrame(string e, string host, int seq, DateTime date, string stat, string info) {
            event_name = e;
            hostname = host;
            timestamp = date.ToString("G");
            sequential_number = seq;
            status = stat;
            additional_info = info;
        }
    }

}