using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using log4net;
using log4net.Config;

using Microsoft.Kinect;

using Newtonsoft.Json;

namespace GestureInterface {

    public class GestureInterface {
        protected static readonly ILog log = LogManager.GetLogger(typeof(GestureInterface));
        private readonly Client _client;
        private readonly GITaskIcon _commands;
        private int _countFrame;
        private readonly KinectSensor _currSensor;
        private readonly Dictionary<ulong, int[]> _handStates;
        private MultiSourceFrameReader _reader;

        private bool _readyForNextGesture;
        private readonly Stopwatch _stopWatchCooldown;
        private readonly GIProperties properties = GIProperties.getInstance();

        private GestureInterface() {
            _commands = new GITaskIcon(this);
            var iconThread = _commands.CreateIconMenuStructure(log);

            if (!GIProperties.getInstance().LOGGING) LogManager.GetRepository().ResetConfiguration();

            if (GIProperties.getInstance().CONNECTION_TEST)
                while (true)
                    Thread.Sleep(1000);

            log.Info("Starting GestureInterface");
            _readyForNextGesture = true;
            _stopWatchCooldown = new Stopwatch();
            _countFrame = 0;
            _handStates = new Dictionary<ulong, int[]>();
            _currSensor = KinectSensor.GetDefault();
            _currSensor.Open();
            _client = new Client(_commands, log);
            log.Warn("Kinect is not connected");
            while (!_currSensor.IsAvailable) Thread.Sleep(100);
            _currSensor.IsAvailableChanged += NotifyAvailableChange;
            _commands.changeKinectState(true);
            log.Info("Kinect connected and ready");
            try {
                _client.Connect();
                _commands.changeServerState(true);
                log.Info("Connected to server");
            }
            catch (IOException e) {
                log.Error(e.Message);
                log.Info("Stopping GestureInterface");
                iconThread.Abort();
                return;
            }

            InitCamera();
            log.Info("Initialization of GestureInterface finished");
            while (true) Thread.Sleep(1000);
        }

        private static void Main(string[] args) {
            if (Directory.Exists(@"KinectSDK"))
                Directory.Delete(@"KinectSDK");
            var gi = new GestureInterface();
        }

        public void ChangeLogggerState(bool activated) {
            GIProperties.getInstance().LOGGING = activated;
            if (activated)
                XmlConfigurator.Configure();
            else
                LogManager.GetRepository().ResetConfiguration();
        }

        private void NotifyAvailableChange(object sender, IsAvailableChangedEventArgs e) {
            if (e.IsAvailable) {
                log.Warn("Kinect connected and ready");
            }
            else {
                log.Info("Kinect disconnected");
                SendConfirmMessageToServer("error", "kinect_disconnected", "Kinect disconnected!");
            }

            _commands.changeKinectState(e.IsAvailable);
        }

        private void InitCamera() {
            _reader = _currSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body);
            _reader.MultiSourceFrameArrived += ReadFrame;
        }

        private void ReadFrame(object sender, MultiSourceFrameArrivedEventArgs e) {
            try {
                //log.Debug("Received Frame from Kinect");
                var sw = new Stopwatch();
                sw.Start();
                //Console.WriteLine("curr_cooldown=" + _currCooldown);
                var af = e.FrameReference.AcquireFrame();
                if (!_readyForNextGesture)
                    if (_stopWatchCooldown.ElapsedMilliseconds > properties.COOLDOWN_GESTURE_DETECTION_MILLISECONDS) {
                        _readyForNextGesture = true;
                        _stopWatchCooldown.Stop();
                        _stopWatchCooldown.Reset();
                    }

                using (var frame = af.BodyFrameReference.AcquireFrame()) {
                    if (frame != null) {
                        var bodies = new Body[frame.BodyFrameSource.BodyCount];
                        frame.GetAndRefreshBodyData(bodies);
                        foreach (var body in bodies)
                            if (body.IsTracked) {
                                CheckBody(body);
                                if (_readyForNextGesture)
                                    if (GestureDetected(body)) {
                                        log.Debug("Confirm-Gesture detected");
                                        _readyForNextGesture = false;
                                        _stopWatchCooldown.Start();
                                        log.Info("Gesture detected -> sending to Server");
                                        SendConfirmMessageToServer("confirm", "ok", "");
                                        log.Info("Confirm-Message sent to Server");
                                    }
                            }
                    }
                }

                sw.Stop();
                var time = (int) sw.Elapsed.TotalMilliseconds;
                if (time < properties.TIME_BETWEEN_FRAMES)
                    Thread.Sleep(properties.TIME_BETWEEN_FRAMES - time);
            }
            catch (Exception ex) {
                log.Error("Exception: " + ex.Message);
                SendConfirmMessageToServer("error", "exception", "Exception: " + ex.Message);
            }
        }

        private void CheckBody(Body body) {
            if (body.HandLeftState != body.HandRightState || body.HandLeftState == HandState.NotTracked ||
                body.HandRightState == HandState.NotTracked || body.HandLeftState == HandState.Unknown ||
                body.HandRightState == HandState.Unknown) { }

            if (_handStates.ContainsKey(body.TrackingId)) {
                for (var i = properties.MAX_LENGTH_HANDSTATE_FIELD - 1; i >= 1; i--)
                    _handStates[body.TrackingId][i] = _handStates[body.TrackingId][i - 1];
                _handStates[body.TrackingId][0] = GetHandStates(body);
            }
            else {
                _handStates.Add(body.TrackingId, new int[properties.MAX_LENGTH_HANDSTATE_FIELD]);
                for (var i = 0; i < properties.MAX_LENGTH_HANDSTATE_FIELD - 1; i++) _handStates[body.TrackingId][i] = 2;
            }
        }

        private string GetStringFromDictionary(ulong id, Dictionary<ulong, int[]> d) {
            var s = "";
            foreach (var v in _handStates)
                if (v.Key == id)
                    foreach (var i in v.Value)
                        s += i + "-";

            return s;
        }

        private bool GestureDetected(Body body) {
            var list = _handStates[body.TrackingId];

            var lastState = 1;
            var timeout = 0;
            var counter = 0;
            for (var i = 0; i <= 9; i++) {
                if (list[i] != lastState && list[i] != 2) {
                    lastState = list[i];
                    timeout = 0;
                    counter++;
                }
                else {
                    timeout++;
                }

                if (timeout > properties.MAX_ERRORS_HANDSTATE)
                    break;

                if (counter == properties.MIN_HANDSTATE_CHANGE_TRIGGER)
                    return true;
            }

            return false;
        }

        private static int GetHandStates(Body body) {
            return body.HandRightState == HandState.Open && body.HandLeftState == HandState.Open ? 0 :
                body.HandRightState == HandState.Closed && body.HandLeftState == HandState.Closed ? 1 : 2;
        }

        private void SendConfirmMessageToServer(string event_name, string gstatus, string message) {
            try {
                var hostname = SystemInformation.ComputerName;

                var frame = new ClientFrame(event_name, hostname, _countFrame++, DateTime.Now, gstatus, message);
                var jsonString = JsonConvert.SerializeObject(frame, Formatting.Indented);

                log.Debug("ClientFrame -> " + jsonString);
                var msg = "";
                try {
                    msg = _client.SendAndReceive(jsonString);
                    log.Debug("Received ServerFrame -> " + msg);
                    _commands.changeServerState(true);
                }
                catch (IOException e) {
                    try {
                        try {
                            _client.Disconnect();
                        }
                        catch (IOException e1) {
                            //
                        }

                        _client.Connect();
                        msg = _client.SendAndReceive(jsonString);
                        log.Debug("Received ServerFrame -> " + msg);
                        _commands.changeServerState(true);
                    }
                    catch (IOException e1) {
                        _commands.changeServerState(false);
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }
    }

}