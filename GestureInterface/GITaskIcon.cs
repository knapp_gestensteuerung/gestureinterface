using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

using log4net;

using Newtonsoft.Json;

namespace GestureInterface {

    public class GITaskIcon {
        private const string KINECT = "Kinect: ";
        private const string SERVER = "Server: ";
        private const string ENABLED = "aktiviert";
        private const string DISABLED = "deaktiviert";
        private const string CONNECTED = "verbunden";
        private const string DISCONNECTED = "getrennt";
        private readonly ContextMenu cmenu = new ContextMenu();

        private readonly GestureInterface gestureInterface;
        private readonly MenuItem kinect = new MenuItem();
        private MenuItem logging;

        private readonly NotifyIcon nicon = new NotifyIcon();
        private readonly MenuItem server = new MenuItem();


        public GITaskIcon(GestureInterface gestureInterface) {
            this.gestureInterface = gestureInterface;
        }

        private Client client { get; set; }
        private string hostname { get; set; }

        public Thread CreateIconMenuStructure(ILog log) {
            var t = new Thread(() => {
                if (GIProperties.getInstance().CONNECTION_TEST)
                    client = new Client(this, log);
                hostname = SystemInformation.ComputerName;

                var close = new MenuItem("&Schließen");
                close.Click += (sender, args) => Environment.Exit(0);


                // MenuItem: commandos
                var commandos = new MenuItem("Befehle");
                string[] commands_text = {"Verbinden", "Trennen", "Sende \"Bestätigung\"", "Sende \"Fehler\""};
                var comm = new MenuItem[commands_text.Length];

                for (var i = 0; i < commands_text.Length; i++) {
                    comm[i] = new MenuItem(commands_text[i]);
                    commandos.MenuItems.Add(comm[i]);
                }

                //  MenuItem commands_connect = new MenuItem("Verbinden");

                // MenuItem: kinect and server
                kinect.Text = KINECT + DISCONNECTED;
                server.Text = SERVER + DISCONNECTED;

                kinect.Enabled = server.Enabled = false;

                var settings = new MenuItem("Einstellungen");
                string[] settings_text = {"Logger ", "Log-Datei öffnen", "Konfigurations-Datei öffen"};
                var sett = new MenuItem[settings_text.Length - 1];

                logging = new MenuItem(settings_text[0]);
                settings.MenuItems.Add(logging);
                for (var i = 1; i < settings_text.Length; i++) {
                    sett[i - 1] = new MenuItem(settings_text[i]);
                    settings.MenuItems.Add(sett[i - 1]);
                }

                logging.Text = settings_text[0] + (GIProperties.getInstance().LOGGING ? DISABLED : ENABLED);

                // Weiter geht -> gestureInterface.ChangeLoggerState(boolean);


                // setting the listener-methods for all commandos
                comm[0].Click += Connect;
                comm[1].Click += Disconnect;
                comm[2].Click += SendConfirm;
                comm[3].Click += SendError;

                // setting the listener-methods for all settings
                logging.Click += changeLoggingState;
                sett[0].Click += openLogFile;
                sett[1].Click += openConfigFile;

                // adding the MenuItems to the MenuContext
                cmenu.MenuItems.Add(commandos);
                cmenu.MenuItems.Add("-");
                cmenu.MenuItems.Add(settings);
                cmenu.MenuItems.Add("-");
                cmenu.MenuItems.Add(close);
                cmenu.MenuItems.Add("-");
                cmenu.MenuItems.Add(kinect);
                cmenu.MenuItems.Add(server);


                nicon.Text = "GestureInterface";
                nicon.Icon = new Icon(@"kinect.ico");
                nicon.ContextMenu = cmenu;
                nicon.Visible = true;

                Application.Run();
            });
            t.Start();
            return t;
        }

        public void Connect(object sender, EventArgs eventArgs) {
            try {
                if (GIProperties.getInstance().CONNECTION_TEST)
                    client.Connect();
            }
            catch (Exception e) {
                // Logger
            }
        }

        public void Disconnect(object sender, EventArgs eventArgs) {
            try {
                if (GIProperties.getInstance().CONNECTION_TEST)
                    client.Disconnect();
            }
            catch (Exception e) {
                // Logger
            }
        }

        public void SendConfirm(object sender, EventArgs eventArgs) {
            try {
                if (GIProperties.getInstance().CONNECTION_TEST) {
                    var frame = new ClientFrame("confirm", hostname, 1, DateTime.Now, "test", "");
                    var jsonString = JsonConvert.SerializeObject(frame, Formatting.Indented);
                    client.SendAndReceive(jsonString);
                }
            }
            catch (Exception e) {
                // Logger
            }
        }

        public void SendError(object sender, EventArgs eventArgs) {
            try {
                if (GIProperties.getInstance().CONNECTION_TEST) {
                    var frame = new ClientFrame("error", hostname, 1, DateTime.Now, "test", "");
                    var jsonString = JsonConvert.SerializeObject(frame, Formatting.Indented);
                    client.SendAndReceive(jsonString);
                }
            }
            catch (Exception e) {
                // Logger
            }
        }

        public void changeLoggingState(object sender, EventArgs eventArgs) {
            gestureInterface.ChangeLogggerState(!GIProperties.getInstance().LOGGING);
            logging.Text = "Logger " + (GIProperties.getInstance().LOGGING ? DISABLED : ENABLED);
        }

        public void openLogFile(object sender, EventArgs eventArgs) {
            Process.Start(@"log\gestureinterface.log");
        }

        public void openConfigFile(object sender, EventArgs eventArgs) {
            Process.Start(@"config\giproperties.ini");
        }

        public void changeKinectState(bool state) {
            kinect.Text = KINECT + (state ? CONNECTED : DISCONNECTED);
        }

        public void changeServerState(bool state) {
            server.Text = SERVER + (state ? CONNECTED : DISCONNECTED);
        }
    }

}